package goutils

import (
	"fmt"
	"testing"
)

func TestLeft(t *testing.T) {
	s := "1234567890"

	v1 := Left(s, 5)
	fmt.Printf("Left(s, 5) = %s\n", v1)
	if v1 != "12345" {
		t.Fatalf("Left(s, 5) != \"12345\"")
	}
}

func TestRight(t *testing.T) {
	s := "1234567890"
	v1 := Right(s, 5)
	fmt.Printf("Right(s, 5) = %s\n", v1)
	if v1 != "67890" {
		t.Fatalf("Right(s, 5) != \"67890\"")
	}
}
