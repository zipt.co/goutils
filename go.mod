module zipt.co/goutils

go 1.22.3

require (
	github.com/soniakeys/meeus/v3 v3.0.1 // indirect
	github.com/soniakeys/unit v1.0.0 // indirect
)
