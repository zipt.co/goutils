package goutils

// Mimic basic left function, with length check
func Left(s string, i int) string {
	if i <= len(s) {
		return s[:i]
	}
	return s
}

func Right(s string, i int) string {
	if i <= len(s) {
		return s[len(s)-i:]
	}
	return s
}
