package goutils

import (
	"fmt"
	"time"
	"github.com/soniakeys/meeus/v3/julian"
)

//                                          1111
//                                01234567890123
const dt_full_format    string = "20060102150405"
const dt_no_secs_format string = "200601021504"
const dt_notime_format  string = "20060102"

func Ballista_to_time(s string) (time.Time, error) {
	format := dt_full_format
	if len(s) <= 12 { format = dt_no_secs_format }
	if len(s) <= 8 { format = dt_notime_format }
	return time.Parse(format, s)
}

// 1977-06-27 19:24:00.000
func String_to_time(s string) (time.Time, error) {
	return time.Parse("2006-01-02 15:04:05", s)
}

func Time_to_ballista(t time.Time) string {
	return t.Format(dt_full_format)
}

func Jd_to_time(jd float64) time.Time {
	return julian.JDToTime(jd)
}

func Time_to_jd(t time.Time) float64 {
	return julian.TimeToJD(t)
	// return julian.CalendarGregorianToJD(t.Year(),
	// 	t.Month(),
	// 	t.Day() + t.Hour()/24.0 + t.Minute()/1440.0 + t.Second()/86400.0)
}

func Time_to_jd2(t time.Time) float64 {
	var y int = int(t.Year())
	var m int = int(t.Month())
	var d float64 = float64(t.Day()) + float64(t.Hour()/24.0) + float64(t.Minute()/1440.0) + float64(t.Second()/86400.0)

	return Dmy_to_jd(y, m, d)
}

func Dmy_to_jd(y int, m int, d float64) float64 {

	s := fmt.Sprintf("%04d%02d%02d", y, m, int(d))
	t, _ := Ballista_to_time(s)
	return Time_to_jd(t)
}

func Dmy_to_jd2(y int, m int, d float64) float64 {
	var jd float64

	if (m < 3) {
		y = y - 1
		m = m + 12
	}

	var a = y / 100
	var b = 0

	if (y > 1582 || (y == 1582 && m > 10 || (m == 10 && d >= 4))) {
		b = 2 - a + (a/4)
	} else {
		b = 0
	}

	var jd1 float64 = float64(365.25) + (float64(y) + 4716.0)
	var jd2 float64 = (float64(30.6001) * (float64(m) + float64(1))) + float64(d) + float64(b) - 1524.5
	jd = float64(int(jd1) + int(jd2))

	return jd
}
