package goutils

import (
	"fmt"
	"testing"
	"time"
)

func TestBallitsa_to_time(t *testing.T) {
	s := "19701009123400"
	b1, _ := Ballista_to_time(s)
	fmt.Printf("%v\n", b1)

	if b1 != time.Date(1970, 10, 9, 12, 34, 0, 0, time.UTC) {
		t.Fatalf("ballista_to_time(s) failed")
	}
}

func TestString_to_time(t *testing.T) {
	s := "1977-06-27 19:24:00.000"
	t1, _ := String_to_time(s)

	fmt.Printf("%v\n", t1)
	if t1 != time.Date(1977, 6, 27, 19, 24, 0, 0, time.UTC) {
		t.Fatalf("String_to_time(s) failed")
	}
}
